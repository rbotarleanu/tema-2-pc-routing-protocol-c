//Nume: Botarleanu Robert-Mihai
//Grupa: 321CB
#include "routing_table.h"
#include <list>
#include <vector>

//construieste o tabela de rutare goala
RoutingTable::RoutingTable(void) {
	size = 0;
}

//construieste o table de rutare cu n rutere
RoutingTable::RoutingTable(const int n){
	size = n;
}

//constructor pentru clonare
RoutingTable::RoutingTable(const RoutingTable& source) {
	size = source.size;
	adjacency = source.adjacency;
	costs = source.costs;
	next_hops = source.next_hops;
}

//copy assignment operator, este respectata rule of three
RoutingTable&
RoutingTable::operator =(const RoutingTable& source){
	//verifica pentru asignare eronata
	if(this != &source) {
		//copiaza listele de adiacenta, costuri si nexthops
		std::vector<Router*> new_adjacency(source.adjacency);	
		std::vector<int> new_costs(source.costs);
		std::vector<Router*> new_nexthops(source.next_hops);

		//le reasigneaza
		adjacency = new_adjacency;
		costs = new_costs;
		next_hops = new_nexthops;
	}
	//intoarce noua identitate
	return *this;
}

//reface drumul de la src la dest folosind prev
int backtrack(std::vector<int> prev, int src, int dest) {
	//daca nu s-a terminat drumul, se continua parcurgerea
	if(src == dest || dest == -1 || prev[dest]==-1) return -1;
	//daca suntem la next_hop de la sursa pentru destinatie, acesta este rezultatul
	if(src == prev[dest]) return dest;
	return backtrack(prev, src, prev[dest]);
}

//calculeaza drumurile minime de la src catre toate nodurile din graf
void dijkstra(std::vector<std::list<std::pair<int, int> > > adjacencies, int n, int src,
	std::vector<int> &dist, std::vector<int> &prev) {
	using namespace std;

	//initial, toate distantele in afara de cea de la src la src sunt Inf
	fill(dist.begin(), dist.end(), Inf);
	fill(prev.begin(), prev.end(), -1);	
	dist[src] = 0;
	//pentru ca priority_queue nu permite modificarea prioritatii imi constuiresc propriul minheap
	priority_queue<pair<int,int>, vector<pair<int,int> >, Comparator> Q;
	
	//de la sursa la sursa distanta e 0
	Q.push(make_pair(src,0));

	while (!Q.empty()) {
		//extrag perechea <key, dist> cea mai mica
		pair<int, int> vp =  Q.top();
		Q.pop();
		
		int u = vp.first;
		
		//cautam drumuri minime via u
		for (list<pair<int, int> >::iterator it = adjacencies[u].begin();
			it != adjacencies[u].end(); it++) {
			//muchia analizata
			pair<int, int> edge = *it;
			//daca drumul poate fi scurtat via nodul u, atunci se acctualizeaza calea
			if (dist[u] + edge.second < dist[edge.first]) {
				dist[edge.first] = dist[u] + edge.second;
				prev[edge.first] = u;
				//se adauga in coada de prioritati, prin metoda asta se evita necesitatea
				//actualizarii prioritatii fiecarui nod din coada
				Q.push(make_pair(edge.first, edge.second));
			}

			//alegerea drumului cu id cel mai mic, trebuie facut traceback pe prev pentru a determina
			//sursa acestui drum si a vedea daca are id mai mic
			if((dist[u] + edge.second == dist[edge.first]) &&
				(backtrack(prev, src,u) < backtrack(prev,src,edge.first))) {
				prev[edge.first] = u;
			}
		}
	}
	std::vector<int> new_prev(prev.size());
	//refac drumurile pentru next_hops
	for(int i = 0; i < n; i++) {
		new_prev[i] = backtrack(prev, src, i);
		if(new_prev[i] == -1) new_prev[i] = src;
	}
	prev = new_prev;
}
