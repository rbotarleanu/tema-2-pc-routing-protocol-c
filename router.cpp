//Nume: Botarleanu Robert-Mihai
//Grupa: 321CB
#include "router.h"
#include <iostream>
//constructor default, un ruter invalid
Router::Router() {
	id = -1;
	table = new RoutingTable();
	topology = new Topology();
}

//construieste ruter-ul cu id-ul id din reteaua cu n rutere
Router::Router(const int id, const int n) {
	this->id = id;
	this->table = new RoutingTable(n);
	this->topology = new Topology(n);
}

//constructorul de clonare
Router::Router(const Router& source) {
	table = new RoutingTable(*(source.table));
	topology = new Topology(*(source.topology));
	this->id = source.id;
}

//copy assignment operator pentru clonare
Router&
Router::operator =(const Router& source) {
	//verifica pentru asignare eronata
	if(this != &source) {
		//asigneaza noile valori ale atributelor
		table = new RoutingTable(*(source.table));
		topology = new Topology(*(source.topology));
		this->id = source.id;
	}
	//intoarce noua identitate
	return *this;
}

//distruge instanta de ruter, distrugand tabela de rutare si topologia
Router::~Router() {
	delete table;
	delete topology;
}
