//Nume: Botarleanu Robert-Mihai
//Grupa: 321CB

//translation unit pentru clasa ruter
#pragma once

class RoutingTable;
class Topology;

#include "routing_table.h"
#include "topology.h"

void hello_router();

class Router{
private:
	int id;
	RoutingTable *table;
	Topology *topology;
public:
	//constructor default, un ruter invalid
	Router(void);
	//construieste ruter-ul cu id-ul id din reteaua cu n rutere
	Router(const int id, const int n);

	//constructor pentru clonare
	Router(const Router& source);

	//copy assignment operator pentru clonare
	Router& operator=(const Router& source);

	//intoarce id-ul ruterului
	int getId(void) { return id; }
	//intoarce tabela de dirijare
	RoutingTable & getTable(void) { return *table; }
	//intoarce topologia
	Topology & getTopology(void) { return *topology; }
	//distruge instanta de ruter, distrugand tabela de rutare si topologia
	~Router();
};