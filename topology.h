//Nume: Botarleanu Robert-Mihai
//Grupa: 321CB

//translation unit pentru clasa topologie
#pragma once

#include <vector>
#include <iostream>
#include <list>
#include <sstream>
#include <string>

void hello_topology();

class Router;

class Topology {
private:
	int size;
	std::vector< std::list< std::pair<int,int> > > adjacencies;
	std::vector<int> versions;

public:
	//constructor pentru topologie goala
	Topology();

	//construieste o topologie de dimensiune n
	Topology(const int n);

	//constructor pentru clonare
	Topology(const Topology& source);

	//copy assignment operator pentru clonare
	Topology& operator=(const Topology& source);

	//intoarce listele de adiacente care formeaza topologia
	std::vector< std::list<std::pair<int,int> > > & getAdjacencies() { return adjacencies; }

	//intoarce versiunile listelor de adiacente
	std::vector<int> & getVersions() { return versions; }

	//intoarce dimensiunea topologiei
	int getSize() { return size; }

	//folosit pentu serializarea unei liste de adiacente
	std::string serialize(int i);
	//rupe legatura dintre r1 si r2
	void remove(int r1, int r2);
};


