Nume: Botarleanu Robert-Mihai
Grupa: 321CB

TEMA 2 PC

	Urmand recomandarea din enunt, am construit 3 clase: Router, RoutingTable si Topology care sunt folosite pentru
a structura informatiile. Clasa Router va retine o tabela de rutare si o topologie. Tabela de rutare va retine un
vector de distante(costuri pentru a ajunge la alte rutere), next_hops si un vector cu toate ruterele. Clasa Topology
poate va avea listele de adiacente si versiune lor.
	In sim.cpp se vor retine vectori cu mesajele si evenimentele, cat si vectorul de rutere si numarul lor. 	
	Pentru rezolvarea cerintelor am scris urmatoarele functii:
	-makeNetwork(char *filename): va citi din messages.in si va initializa reteaua de rutere
	-floodNetwork(): va anunta fiecare ruter sa inceapa floodarea retelei cu informatiile pe care le are(despre proprii
vecini). Pentru floodare exista o functie care anunta un ruter sa isi trimita lista de adiacenta la vecinii sai, numita
floodRouter(int i), unde i este id-ul ruterului.
	-floodRouter(int i): ruterul i isi va anunta vecinii de informatiile proprii(propria lista de adiacenta)
	-makeRoute(int r1, int r2, int cost): va crea o ruta intre ruterele r1 si r2 de cost, cu verificarea sa nu existe
deja ruta(caz in care se sterge si se suprascrie)
	-removeRoute(int r1, int r2): va sterge ruta dintre r1 si r2

	Init_sim() va construi mai intai reteaua cu makeNetwork(), va incepe floodarea initiala cu floodNetwork() si va
incarca mesajele si evenimentele cu loadMessages si loadEvents in vectorii de evenimente si mesaje.
	In oglinda, clean_sim() va sterg reteaua de rutere din memorie.

	trigger_events() va parcurge evenimentele din vectorul de evenimente si va vedea daca exista evenimente ce trebuiesc
declansate dupa, in functie de tipul evenimentului va apela makeRoute si removeRoute pentru a crea/rupe legatura si a
incepe floodarea cu mesaje de protocol.

	Pentru procesarea mesajelor sunt 3 etape: verificare daca trebuie sa porneasca un mesaj prin retea, verificare mesaje
de rutare si verificare mesaje de protocol. Astfel:
	-tigger_messages() va lucra intr-o maniera similara cu trigger_events(), parcurgand vectorul de mesaje si verificand
daca trebuie declansat vreun mesaj la momentul curent de simulare. Ruterul sursa va trimite la destinatie mesajul,
rutand prin nextHop.
	- se verifica daca mai exista mesaje de rutare de primit, daca da se verifica daca ruterul curent este destinatia caz
	in care se opreste rutarea. Altfel, se va trimite prin nextHop catre destinatie.
	- se verifica daca mai sunt mesaje de protocol de primit, daca da se verifica versiunea informatiei. In cazul in care
	informatia este mai veche sau aceeasi cu cea retinuta de topologia ruterului atunci nu se mai face forward. Altfel,
	se trimit mesajele mai departe catre vecinii ruterului.

	Pentru actualizarea tabelului de rutare, am folosit algoritmul lui dijkstra cu coada minima de prioritati, introducand
in coada perechi de forma (id, lungime_pana_la_sursa) si, prin intermediul unui comparator, ordinea este determinata in
primul rand de costul de a ajunge de la sursa la nodul curent si in al doilea rand de id. Pentru a alege drumul cu id 
minim, a trebuit sa reconstruiesc calea pana la un nod si sa vad daca ruta data de noul nod intermediar candidat pornea
cu un id mai mic. 



