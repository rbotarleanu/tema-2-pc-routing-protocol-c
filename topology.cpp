//Nume: Botarleanu Robert-Mihai
//Grupa: 321CB
#include "topology.h"
#include <iostream>

//constructor pentru topologie goala
Topology::Topology(void): adjacencies(0), versions(0) {
	size = 0;
}

//construieste o topologie de dimensiune n
Topology::Topology(const int n): adjacencies(n), versions(n) {
	size = n;	
	//ruterul nu contine informatii despre topologii, orice informatie este mai noua decat ce are
	fill(versions.begin(), versions.end(), -1);
}

//constructor pentru clonare
Topology::Topology(const Topology& source) {
	size = source.size;
	adjacencies = source.adjacencies;
	versions = source.versions;
}

//copy assignment operator pentru clonare
Topology&
Topology::operator =(const Topology& source) {
	//verifica pentru asignare eronata
	if(this != &source) {
		//asigneaza noile valori
		std::vector< std::list< std::pair<int,int> > > new_adjacencies(source.adjacencies);
		std::vector<int> new_versions(source.versions);

		this->size = source.size;	
		this->adjacencies = new_adjacencies;
		this->versions = new_versions;
	}
	//intoarce noua identitate
	return *this;
}

//folosit pentu serializarea unei liste de adiacente
std::string
Topology::serialize(int i) {
	std::stringstream s;
	s << versions[i] << ' ';
	s << i << ' ';
	for(std::list<std::pair<int,int> >::iterator it = adjacencies[i].begin();
		it != adjacencies[i].end(); it++) {
		std::pair<int,int> vertex = *it;
		s << vertex.first << ' ' << vertex.second << ' ';
	}
	return s.str();
}

//rupe legatura dintre r1 si r2
void
Topology::remove(int r1, int r2) {
	//se parcurge lista de adiacenta si se construieste una noua in care nu mai apare legatura cu r2
	std::vector<std::pair<int,int> > aux;
	for(std::list<std::pair<int,int> >::iterator it = adjacencies[r1].begin();
		it != adjacencies[r1].end(); it++) {
		std::pair<int,int> vertex = *it;
		if(vertex.first != r2) aux.push_back(vertex);
	}
	adjacencies[r1].clear();
	adjacencies[r1] = std::list<std::pair<int,int> >(aux.begin(), aux.end());
}
