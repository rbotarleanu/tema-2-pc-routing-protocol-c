#!/bin/bash
cd ..
./simulation tests/input/test4/topology.in tests/input/test4/messages.in tests/input/test4/events.in
mv results.out tests/results/test4/
cat tests/results/test4/results.out | sort > tests/results/test4/results_sorted.out

DIFF_RESULTS=$(diff tests/output/test4/results_sorted.out tests/results/test4/results_sorted.out)
if [ "$DIFF_RESULTS" != "" ] 
then
    echo "TEST 4 - FAIL"
else
	echo "TEST 4 - PASS"
fi

