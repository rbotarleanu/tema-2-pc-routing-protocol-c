#!/bin/bash
cd ..
./simulation tests/input/test1/topology.in tests/input/test1/messages.in tests/input/test1/events.in
mv results.out tests/results/test1/
cat tests/results/test1/results.out | sort > tests/results/test1/results_sorted.out

DIFF_RESULTS=$(diff tests/output/test1/results_sorted.out tests/results/test1/results_sorted.out)
if [ "$DIFF_RESULTS" != "" ] 
then
    echo "TEST 1 - FAIL"
else
	echo "TEST 1 - PASS"
fi
