#!/bin/bash
cd ..
./simulation tests/input/test3/topology.in tests/input/test3/messages.in tests/input/test3/events.in
mv results.out tests/results/test3/
cat tests/results/test3/results.out | sort > tests/results/test3/results_sorted.out

DIFF_RESULTS=$(diff tests/output/test3/results_sorted.out tests/results/test3/results_sorted.out)
if [ "$DIFF_RESULTS" != "" ] 
then
    echo "TEST 3 - FAIL"
else
	echo "TEST 3 - PASS"
fi

