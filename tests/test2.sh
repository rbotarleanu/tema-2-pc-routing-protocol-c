#!/bin/bash
cd ..
./simulation tests/input/test2/topology.in tests/input/test2/messages.in tests/input/test2/events.in
mv results.out tests/results/test2/
cat tests/results/test2/results.out | sort > tests/results/test2/results_sorted.out

DIFF_RESULTS=$(diff tests/output/test2/results_sorted.out tests/results/test2/results_sorted.out)
if [ "$DIFF_RESULTS" != "" ] 
then
    echo "TEST 2 - FAIL"
else
	echo "TEST 2 - PASS"
fi

