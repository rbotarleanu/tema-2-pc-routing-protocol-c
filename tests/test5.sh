#!/bin/bash
cd ..
./simulation tests/input/test5/topology.in tests/input/test5/messages.in tests/input/test5/events.in
mv results.out tests/results/test5/
cat tests/results/test5/results.out | sort > tests/results/test5/results_sorted.out

DIFF_RESULTS=$(diff tests/output/test5/results_sorted.out tests/results/test5/results_sorted.out)
if [ "$DIFF_RESULTS" != "" ] 
then
    echo "TEST 5 - FAIL"
else
	echo "TEST 5 - PASS"
fi

