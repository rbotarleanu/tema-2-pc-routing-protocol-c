/******************************************************************************/
/* Tema 2 Protocoale de Comunicatie (Aprilie 2015)                            */
/******************************************************************************/

#include "sim.h"
#include "router.h"
#include "api.h"

#include <fstream>
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <vector>
#include <cstring>
#include <sstream>

//numarul de rutere
int n = 0;
//reteaua de rutere
std::vector<Router*> routers;
//mesaje si evenimente
std::vector<std::string> msgs;
std::vector<std::string> events;

//initializeaza reteaua de rutere
void makeNetwork(char *filename);
//realizeaza floodarea initiala a retelei(fiecare ruter isi trimite lista de adiacenta la vecini)
void floodNetwork();
//ruterul i va trimite vecinilor sai propria lista de adiacenta
void floodRouter(int i);
//construieste cale intre r1 si r2 de cost precizat
void makeRoute(int r1, int r2, int cost);
//rupe legatura dintre r1 si r2
void removeRoute(int r1, int r2);

//proceseaza mesajele de protocol pentru ruterul i
int process_protocol_message(int i);
//incarca mesajele si evenimentele pentru a le putea declansa
void loadMessages(char *fname);
void loadEvents(char *fname);

void init_sim(int argc, char **argv) {
  makeNetwork(argv[1]);
  floodNetwork();
  loadMessages(argv[2]);
  loadEvents(argv[3]);
}

void clean_sim() {
  //se dealoca memoria pentru fiecare ruter.
  for(int i = 0; i < n; i++) 
  	{ 
      delete routers[i];
    }
}

void trigger_events() {
 //se parcurg evenimentele si se verifica daca vreunul treubie declansat la momentul curent de timp
 for(unsigned int i = 0; i < events.size(); i++) {
  	//format mesaj
    int r1, r2, type, cost, tstamp;
  	std::stringstream evnt(events[i]);
  	evnt >> r1 >> r2 >> type >> cost >> tstamp;
  	if(tstamp != get_time()) continue;
  	if(r1 == -1 || r2 == -1) continue;
  	//daca type==1, atunci se sterge ruta r1->r2(si r2->r1), altfel se construieste
    if(type == 1) {
  		removeRoute(r1,r2);	
  	} else {
  		makeRoute(r1,r2, cost);	
  	}
  }
}

//declanseaza trimiterea mesajelor daca corespunde vreunul cu momentul curent de simulare
void triggerMessages() {
	for(unsigned int i = 0; i < msgs.size(); i++) {
  	//formatul mesajului
    int srcId=-1, dstId=-1, tStamp,tag;
  	char content[MAX_MSG_CONTENT];
  	std::stringstream msg(msgs[i]);
  	msg>> srcId >> dstId >> tStamp >> tag;
   	if(srcId == -1 || dstId == -1) {
 	  	continue;
   	}
    //citesc mesajele linie cu linie
  	msg.getline(content, MAX_MSG_CONTENT);
  	char *send = (char*)calloc(strlen(content)+2, sizeof(char));
  	strcpy(send, content);
    //getline citeste pana la \n, il concatenez pentru pastrarea mesajului
  	strcat(send, "\n");
    //daca mesajul curent se declanseaza la momentul curent de simulare
  	if(tStamp == get_time()) {
      //incep rutarea din sursa, catre destinatie, prin aflarea nextHop-ului
  		int nextHop = routers[srcId]->getTable().getNext_hops().at(dstId)->getId();
  		endpoint[srcId].route_message(&endpoint[nextHop], dstId, tag, send, NULL);
  	}
  	free(send);
  }
}

void process_messages() {
  //declansez mesajele ce trebuie trimise
  triggerMessages();
  
  //analizez pentru fiecare ruter mesajele primite
  for(int i = 0; i < n; i++) {
    //verific daca mai ruterul i mai are mesaje de rutare de primit
  	while(endpoint[i].are_there_any_messages_left()) {
		int src = -1, dst = -1, tag;
		char *msg = new char[MAX_MSG_CONTENT];
		bool valid = endpoint[i].recv_message(&src, &dst, &tag, msg);
		//daca nu mai primeste mesaj valid, inseamna ca nu mai are mesaje de rutare de primit
    if(!valid) { delete[] msg; break; }
		//daca ruterul curent nu este destinatia
    if(dst != i) {
      //determin urmatorul hop si trimit mai departe
			int nextHop = routers[i]->getTable().getNext_hops().at(dst)->getId();
			endpoint[i].route_message(&endpoint[nextHop], dst, tag, msg, NULL);
		} else {
      //altfel ruterul curent este destinatia finala si nu il mai trimit mai departe
      delete[]msg;
      continue;
    }
		delete[] msg; 
	}
  //verific daca ruterul i mai are mesaje de protocol de primit si realizez forward
	while(endpoint[i].are_there_any_messages_left()) {
	    if(process_protocol_message(i) == -1) break;
	}
 }
}

void update_routing_table() {
  //se foloseste algoritmul lui dijkstra pentru fiecare ruter si se determina tabela de rutare
  for(int i = 0; i < n; i++) {
    std::vector<int> dist(n), prev(n);
    //sterg informatiile precedente
    routers[i]->getTable().getAdjacency().clear();
    routers[i]->getTable().getCosts().clear();
    routers[i]->getTable().getNext_hops().clear();
    
    //calculez drumurile minime catre restul ruterelor (pornind de la ruterul curent)
    dijkstra(routers[i]->getTopology().getAdjacencies(), n, i, dist, prev);
    //pentru toate ruterele
    for(int j = 0; j < n; j++) {
      //adaug informatiile despre drumuri si nexthops
      routers[i]->getTable().getAdjacency().push_back(routers[j]);
      routers[i]->getTable().getCosts().push_back(dist[j]);
      //daca nu se poate ajunge la ruterul j, nextHop e NULL(in principiu, pentru a ajunge la sine)
      if(prev[j] != -1)  {
        routers[i]->getTable().getNext_hops().push_back(routers[prev[j]]);
      } else {
        routers[i]->getTable().getNext_hops().push_back(NULL);
      }
    }
  }
}

//initializeaza reteaua de rutere, fiecare ruter isi va cunoaste doar vecinii
void makeNetwork(char *filename) {
  //deschiderea fisierului de topologie si verificarea.
  std::ifstream fin(filename);
  if(!fin) {
  	perror("Invalid topology file. Exiting");
  	exit(0);
  }

  //dimensiunea retelei
  fin >> n;

  //aloc reteaua de routere, si topologia/tabela pentru fiecared
  routers = std::vector<Router*>(n);
  for(int i = 0; i < n; i++) {
  	//ruter cu id "i" dintr-o retea cu "n" rutere
  	routers[i] = new Router(i, n);
  }

   //se afla initial vecinii pe care ii cunoaste fiecare ruter
   int src, cost, dest;
    while((fin >> src >> cost >> dest) != NULL) {
   		//fiind valorile cunoscute initial, versiunea lor este 0 si nu se va updata niciodata la floodare
      //in caz ca apare aceeasi muchie de doua ori in fisier, se scoate inainte de inserare
      routers[src]->getTopology().remove(src,dest);
      routers[src]->getTopology().getAdjacencies().at(src).push_back(std::pair<int,int>(dest,cost));
      routers[src]->getTopology().getVersions().at(src) = 0;
      //si adiacenta inversa
      routers[dest]->getTopology().remove(dest, src);
      routers[dest]->getTopology().getAdjacencies().at(dest).push_back(std::pair<int,int>(src,cost));
      routers[dest]->getTopology().getVersions().at(dest) = 0;
    }
   fin.close();
}

void floodNetwork() {
  //fiecare ruter isi va serializa propria lista de adiacenta si o va trimite vecinilor
  for(int i = 0; i < n; i++) {
   	floodRouter(i);
  }
}

//proceseaza mesajele de protocol pentru ruterul i
int process_protocol_message(int i) {
		char *msg = (char*)calloc(MAX_MSG_CONTENT, sizeof(char));
    int response = endpoint[i].recv_protocol_message(msg);
    //a primit toate mesajele de gestionare
    if(response == -1){ free(msg); return -1; }
  
    //se preia mesajul de gestionare
    std::stringstream inmsg(msg);
    int version, src, cost, dest;
    inmsg >> version >> src;
    if(routers[i]->getTopology().getVersions().at(src) >= version) {
    	//informatia este aceeasi sau mai veche, nu se actualizeaza/trimite mai departe
    	free(msg);
    	return 1;
    }
    routers[i]->getTopology().getVersions().at(src) = version;
    routers[i]->getTopology().getAdjacencies().at(src).clear();
    //se actualizeaza informatia din lista de adiacenta
    while((inmsg >> dest >> cost) != NULL) {
      routers[i]->getTopology().getAdjacencies().at(src).push_back(std::pair<int,int>(dest,cost));
    }
    //se trimite mai departe mesajul la toti vecinii
    for(std::list<pair<int,int> >::iterator it = routers[i]->getTopology().getAdjacencies().at(i).begin();
      it != routers[i]->getTopology().getAdjacencies().at(i).end(); it++) {
	    int neighbour = (*it).first;
	       endpoint[i].send_msg(&endpoint[neighbour], msg, inmsg.str().length(), NULL);
      }
    free(msg);
    return 0;
}

//incarca mesajele in vectorul de mesaje
void loadMessages(char *fname) {
	std::ifstream fin(fname);
	int m;
	fin >> m;
	char mesaj[MAX_MSG_CONTENT];
  //un getline gol pentru a trece de "\n"
	fin.getline(mesaj, MAX_MSG_CONTENT);
	for(int i = 0; i <= m; i++) {
    //citesc mesajul
		fin.getline(mesaj, MAX_MSG_CONTENT);
    //concatenez \n la sfarsit pentru ca getline nu il citeste
		strcat(mesaj, "\n\0");
    //adaug in vectorul de mesaje
		msgs.push_back(std::string((const char*)mesaj));
	} 
	fin.close();
}

//incarca evenimentele in vectorul de evenimente
void loadEvents(char *fname) {
	std::ifstream fin(fname);
	int n,m;
	fin >> n>> m;
	char event[MAX_MSG_CONTENT];
  //getline gol pentru a trece de \n
	fin.getline(event, MAX_MSG_CONTENT);
		
	for(int i = 0; i <= m; i++) {
    //evenimentele se citesc si se adauga in vectorul de evenimente
		fin.getline(event, MAX_MSG_CONTENT);
		events.push_back(std::string((const char*)event));
	} 
	fin.close();
}

//ruterul i isi floodeaza vecinii cu informatiile proprii
void floodRouter(int i) {
  //ruterul trimite serializarea propriei liste de adiacenta la toti vecinii sai
	std::string serialization = routers[i]->getTopology().serialize(i);
  char *msg = strdup(serialization.c_str());
  for(std::list<pair<int,int> >::iterator it = routers[i]->getTopology().getAdjacencies().at(i).begin();
      it != routers[i]->getTopology().getAdjacencies().at(i).end(); it++) {
      int neighbour = (*it).first;
      endpoint[i].send_msg(&endpoint[neighbour], msg, serialization.size(), 0);
    }
  free(msg);
}

//elimina ruta dintre ruterele r1 si r2, stergand din topologia fiecaruia legaturile
void removeRoute(int r1, int r2) {
	routers[r1]->getTopology().remove(r1,r2);
	routers[r1]->getTopology().getVersions().at(r1)++;
	routers[r2]->getTopology().remove(r2,r1);
	routers[r2]->getTopology().getVersions().at(r2)++;
  //ruterele isi anunta vecinii si incepe o noua floodare a retelei cu mesaje de protocol
	floodRouter(r1);
	floodRouter(r2);
}

void makeRoute(int r1, int r2, int cost) {
  //se adauga noua legatura r1<->r2 de cost precizat in topologiile ambelor rutere
	routers[r1]->getTopology().getAdjacencies().at(r1).push_back(std::make_pair(r2, cost));
	routers[r1]->getTopology().getVersions().at(r1)++;
	routers[r2]->getTopology().getAdjacencies().at(r2).push_back(std::make_pair(r1, cost));
	routers[r2]->getTopology().getVersions().at(r2)++;
  //ruterele isi anunta vecinii si incepe o noua floodare a retelei cu mesaje de protocol
	floodRouter(r1);
	floodRouter(r2);
}
