//Nume: Botarleanu Robert-Mihai
//Grupa: 321CB

//translation unit pentru clasa tabela de rutare
#pragma once

#include <list>
#include <iostream>
#include <fstream>
#include <limits>
#include <queue>
#include <vector>
#include <string>

#define Inf std::numeric_limits<int>::max()

class Router;

#include "router.h"

//o clasa de comparator pentru dijkstra, va sorta perechile de tip (id, distanta la sursa) dupa
//distanta(si la distante egale dupa id)
class Comparator {
public:
	bool operator()(std::pair<int, int> p1, std::pair<int, int>p2) {
		return p1.second > p2.second;
		if(p1.second == p2.second) return p1.first > p2.first;
		return false;
	}
};

//apeleaza algoritmul lui dijkstra pentru listele de adiacente, numarul de noduri, sursa si
//intoarce doi vectori: de distante si de next_hops
void dijkstra(std::vector<std::list<std::pair<int, int> > > adjacencies, int n, int src,
	std::vector<int> &dist, std::vector<int> &prev);
void hello_routing_table();

class RoutingTable {
private:
	int size;
	std::vector<Router*> adjacency;
	std::vector<int> costs;
	std::vector<Router*> next_hops;
public:
	//construieste o tabela de rutare goala
	RoutingTable(void);
	//construieste o table de rutare cu n rutere
	RoutingTable(const int n);
	
	//constructor pentru clonare
	RoutingTable(const RoutingTable& source);

	//copy assignment operator, este respectata rule of three
	RoutingTable &operator =(const RoutingTable& source);

	//intoarce dimensiunea tabelului
	int getSize() { return size; }

	//intoarce lista de adiacenta pentru rutere
	std::vector<Router*> & getAdjacency() { return adjacency; }
	//intoarce lista pentru next hops
	std::vector<Router*> & getNext_hops() { return next_hops; }
	//intoarce lista de costuri
	std::vector<int> & getCosts() { return costs; }

};